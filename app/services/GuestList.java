package services;



import models.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jakub on 25.08.16.
 */
public class GuestList { //alt+enter - importuje paczke

    private List<Guest> GuestList = new ArrayList<>();
    public GuestList() {

        Guest guest1 = new Guest();
        guest1.setName("Jack Black");
        guest1.setAge((byte) 30);


        Guest guest2 = new Guest();
        guest2.setName("John Snow");
        guest2.setAge((byte) 21);

        //Guest guest4 = new Guest();
        //guest4.setName("Michał");
        //guest4.setAge(30);

        GuestList.add(guest1); //ctr + D duplikacja
        GuestList.add(guest2);
        //GuestList.add(guest4);
    }
    public void addGuest(String newName, byte newAge) {
        Guest guest2 = new Guest();
        guest2.setName(newName);
        guest2.setAge(newAge);
        this.GuestList.add(guest2);
    }
    public Guest find (Long id){
        final Guest[] guestFound = new Guest[1];   // final[...] - nie da się później tego zmienić.
        getGuestList().forEach((g) -> {  // pobieram właściwą listę gości. forEach -> (Dla każðego g) (g) ->
            if(g.getId().equals(id)) {              // g.getId().equals
                //if(g.getId().longValue()==id.longValue()) {              // unboxing
                guestFound[0] =g;                       // dla guestFound[0] znajduje ten element któ©y można porównać.
            }
        });
        return guestFound[0];
    }
    public Guest delete (Long id){
        final Guest[] guestFound = new Guest[1];   // final[...] - nie da się później tego zmienić.
        getGuestList().forEach((g) -> {  // pobieram właściwą listę gości. forEach -> (Dla każðego g) (g) ->
            if(g.getId().equals(id)) {              // g.getId().equals
                //if(g.getId().longValue()==id.longValue()) {              // unboxing
                guestFound[0] =g;                       // dla guestFound[0] znajduje ten element któ©y można porównać.
            }
        });
        return guestFound[0];
    }
    public void addGuest(Guest guest){
        this.GuestList.add(guest);
    }
    public void removeGuest(int pusty) { //methody, nazwy, itd. routing (w plioku routes? get przekazuje parametry w URL, post parametry w treści.
        this.GuestList.remove(pusty);
    }
    public void removeGuest(Long id){
        Guest todelete = find(id); // użyć metody guest find
        GuestList.remove(todelete); // gość "to delete"
    }

    public List<Guest> getGuestList() {
        return GuestList;
    }

    public void setGuestList(List<Guest> GuestList) {
        this.GuestList = GuestList;
    }
}
