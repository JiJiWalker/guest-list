package models;

public class Guest {
    private String name;
    private byte age;
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getAge() {

        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    } // pull push commit merch

}
