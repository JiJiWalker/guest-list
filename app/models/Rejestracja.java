package models;

/**
 * Created by jakub on 18.08.16.
 */
public class Rejestracja {

    private String imie;
    private String nazwisko;


    public Rejestracja(String imie, String nazwisko) { //alt + insert
        this.imie = imie;
        this.nazwisko = nazwisko;
    }


    public String getImie() {

        return imie;
    }

    public void setImie(String imie) {

        this.imie = imie;
    }

    public String getNazwisko() {

        return nazwisko.toUpperCase(); //zwraca dużymi literami
    }

    public void setNazwisko(String nazwisko) {

        this.nazwisko = nazwisko;
    }

    public String imienazwisko(){
        String nazwisko = "dupa"; //nazwisko bez this zwracam nazwisko okreśłone jako "dupa" (lokalnie pomiędzy klamrami)
        return this.imie+this.nazwisko; //z this. zwraca z klamry na górze czyli to co musi uzuoełnić użytkownik.
        // Scoup - zakres w ktorym coś obowiązuje czyli lokalnie między klamrami albo szerzej.
    }
}
