package controllers;

import services.GuestList;
import play.libs.Json;
import play.mvc.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller { // klasa zbiór metod i włąściości.
    public String komunikat = "Witam i działam"; // metoda - zbiór kodu / typ danych (co z powrotem dostanę jak to zrobię) / nazwa / co przyjmuę
    private GuestList jakasLista = new GuestList();
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() { //Result wszystkie fukncje które chce wyświetlić się zwracają.
        jakasLista.addGuest("puste miejsce",(byte) 0);
        return ok(Json.toJson(jakasLista)); //json format danych.
    }

    public Result dodaj(String name, byte age) {
        jakasLista.addGuest(name,age);
        return ok(Json.toJson(jakasLista));
    }

    public Result usun(Integer index) {
        jakasLista.removeGuest(index);
        return ok("usunięto");

    }


    public int Dodaj(int liczba1, int liczba2) {
        return liczba1+liczba2;
        // po co są construktory, gettery, settery, instancje klasy i dziedziczenie (coś co z siebie wynika np listę gości a lista zawiera osoby. lista gości sklada się z ze mnie i mojej kobiety (2 osoby w konstruktorze) dodać je do listy wewnętrzej a później żeby móć dodawać nowe osoby.
    }

}
