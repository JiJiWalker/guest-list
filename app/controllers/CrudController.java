package controllers;


import models.Guest;
import play.libs.Json;
import play.mvc.Result;
import services.GuestList;
import play.mvc.Controller;

public class CrudController extends Controller {

    GuestList guestList = new GuestList(); // utworzy dla tej zmiennej nową listę gośći

    public Result createGuest(Long id, String name, Byte age) { //z dużych liter
        Guest g = new Guest();
        g.setId(id);
        g.setName(name);
        g.setAge(age);
        guestList.addGuest(g);
        return ok("Stworzono.");
    }

    public Result readGuest(Long id) {

        return ok(Json.toJson(guestList.find(id)));
    }

    public Result updateGuest(Long id, String name) {
        Guest e = guestList.find(id); //nowy gość bo nowe dane.
        e.setName(name);
        return ok("Zmieniono");
    }

    public Result deleteGuest(Long id) {
        guestList.removeGuest(id);
        return ok("Usunięto");
    }



}